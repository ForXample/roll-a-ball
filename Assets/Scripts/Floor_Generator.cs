﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Floor_Generator : MonoBehaviour
{
    // We need to keep a reference to all these objects
    List<GameObject> floorObjects;

    int nextFloorToDrop = 0;

    public float timerLength; // Assuming one second, can be more or less

    public float currentTimer; // The current timer

    // Start is called before the first frame update
    [Header("floorInfo")]
    public GameObject floor;
    public int length;
    public int width;
    [Header("locoInfo")]
    public Transform generatorPoint;

    // Start is called before the first frame update
    void Start()
    {
        floorObjects = new List<GameObject>();

        for (int z = 0; z < length; z++)
        {
            for (int x = 0; x < width; x++)
            {
                Vector3 location = new Vector3(x, 0, z);
                GameObject temp = Instantiate(floor);
                temp.transform.position = location;
                floorObjects.Add(temp);
            }
        }


    }

    // Update is called once per frame
    void FixedUpdate()
    {
        timer();
    }

    public void timer()
    {
        // You need a timer. I am going to ignore that for one second.

        // Let's say the time period has passed and we want to make the next floor segment drop

        currentTimer -= Time.deltaTime; // Decrease your timer by the amount of time between frames

        // Yes in this implementation the timer keeps going even if we're run out of floor. Best practice would probably be to have a boolean or something that says "Don't count down anymore"
        // But I'm not very concerned about that in this implementation

        if (currentTimer <= 0) // We count down to zero, when we hit zero or less, we make the enxt floor drop, and reset the timer
        {
            currentTimer = timerLength;

            // Now you need a timer
            if (nextFloorToDrop < (length * width)) // Only do this if there are any left to drop
            {
                // This grabs the rigidbody from floor that needs to drop

                Rigidbody rb = floorObjects[nextFloorToDrop].GetComponent<Rigidbody>();

                // Start with useGravity unchecked in the inspector
                rb.useGravity = true;
                rb.isKinematic = false;

                nextFloorToDrop += 1; // Set it to the next floor to drop
            }
        }
    }
}

