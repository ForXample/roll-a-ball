﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;


public class Player_Controller : MonoBehaviour
{



    public float speed = 0;

    private Rigidbody rb;

    private float movementX;
    private float movementY;
    // Start is called before the first frame update

    void Start()
    {

        rb = GetComponent<Rigidbody>();

    }

    // gets the Vector2 data from the movementValue and stores it in a Vector2 variable I'm creating called movementVector
    void OnMove(InputValue movementValue)
    {
        Vector2 movementVector = movementValue.Get<Vector2>();

        movementX = movementVector.x;
        movementY = movementVector.y;
    }



    void FixedUpdate()
    {
        Vector3 movement = new Vector3(movementX, 0.0f, movementY);

        rb.AddForce(movement * speed);
    }


}
